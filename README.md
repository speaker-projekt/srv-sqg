# srv-sqg
 SPARQL query generator for QA

## How to run docker:

```
docker build -t speaker-srv-sqg .
docker run -p 5000:5000 -it speaker-srv-sqg
```

## Input

`curl -i -H "Content-Type: application/json" -X POST -d '{"question":"What is the hometown of Nader Guirat, where Josef Johansson was born too?","relations":[{"surface":"","uris":[{"confidence":1,"uri":"http://dbpedia.org/property/birthPlace"}]},{"surface":"","uris":[{"confidence":1,"uri":"http://dbpedia.org/ontology/hometown"}]}],"entities":[{"surface":"","uris":[{"confidence":0.7,"uri":"http://dbpedia.org/resource/Josef_Johansson"},{"confidence":0.3,"uri":"http://dbpedia.org/resource/Barack_Obama"}]},{"surface":"","uris":[{"confidence":1,"uri":"http://dbpedia.org/resource/Nader_Guirat"}]}],"kb":"dbpedia"}' http://localhost:5000/srv-sqg/query`

## Output

`{"queries":[{"confidence":0.10000000000000009,"query":" SELECT DISTINCT ?u_0 WHERE { <http://dbpedia.org/resource/Josef_Johansson> <http://dbpedia.org/property/birthPlace> ?u_0 .<http://dbpedia.org/resource/Nader_Guirat> <http://dbpedia.org/ontology/hometown> ?u_0 }"}],"type":"list","type_confidence":0.9061662860114019}`



