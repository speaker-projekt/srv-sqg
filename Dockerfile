FROM python:3.8.6
LABEL maintainer="elena.leitner@dfki.de"


RUN apt-get -y update && \
    apt-get upgrade -y && \
    apt-get install -y python3-dev &&\
    apt-get update -y


ADD SQG-master/requirements.txt .
RUN pip install -r requirements.txt

ADD SQG-master .


ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8

EXPOSE 5000

ENTRYPOINT FLASK_APP=flaskController.py flask run --host=0.0.0.0 --port=5000
#CMD ["/bin/bash"]
